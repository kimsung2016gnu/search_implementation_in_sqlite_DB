package org.mytriangle.com.searchinginsqlitedatabase;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements SearchView.OnSuggestionListener{
    SqliteHandler myDatabase;
    SearchView searchView;

    EditText editName, editAge;
    Button btnInsert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDatabase = new SqliteHandler(MainActivity.this);

        editName = (EditText) findViewById(R.id.editName);
        editAge = (EditText) findViewById(R.id.editAge);

        btnInsert = (Button) findViewById(R.id.btnToInsert);

        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataForInserting();
            }
        });

    }

    private void getDataForInserting() {
        String name = editName.getText().toString();
        String age = editAge.getText().toString();

        myDatabase.addEmployess(name,age);
        editName.setText("");
        editAge.setText("");
        Toast.makeText(getApplicationContext(), "Inserted", Toast.LENGTH_LONG).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        doSearchActivateSearch(menu);

        return true;
    }


    private void doSearchActivateSearch(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        final SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, EmployeesSearchableActivity.class)));
        searchView.setOnSuggestionListener(this);
        searchView.setIconifiedByDefault(false);
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return true;
    }

    @Override
    public boolean onSuggestionClick(int position) {
        int id = (int) searchView.getSuggestionsAdapter().
                getItemId(position);

        Intent intent = new Intent(this, EmployeesSearchableActivity.class);

        intent.putExtra("id", id);

        startActivity(intent);

        return true;
    }
}
